+++
created = 2024-12-30
updated = 2024-01-11
date = 2024-12-21
title = "Rentekstbasert regnskapsføring med hledger"
+++

Et [rentekstbasert](/musings/plaintext-tools) for personlig regnskapsføring. Sannsynligvis nok et tilfelle av [monomani](/musings/monomani), men er et grep jeg tar for å få litt oversikt over øknomien.

Per akkurat nå føres alt opp manuelt.

Se [hledger.org](https://hledger.org/).
