+++
created = 2024-12-30
updated = 2024-12-30
date = 2024-12-30
title = "Å sette opp et utviklermiljø for nettsiden"
+++

Denne nettsiden er nå satt opp med `nix` (med `flakes`) opp et efemerisk uviklermiljø hvor den installerer `zola` (nettside-motoren) og `zed` (editor).

Et slags mål er å sette opp et miljø hvor editoren settes opp med ulike tillegg og innstillinger unikt for hvert prosjekt.
