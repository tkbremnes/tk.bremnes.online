+++
title = "Bidrag"
created = 2024-11-19
updated = 2024-12-05
+++

## Oversettelser
- OpenFoodFacts
- Kagi
- Kvaesitso
- Fedilab
- Phanphy
- BookWyrm
- Capy Reader

## Annet
- Wikipedia
- Wikidata
- OpenStreetMap
- Open Library
- OpenFoodFacts

## Kode
- BookWyrm
