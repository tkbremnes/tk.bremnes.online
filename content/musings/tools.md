+++
title = "Verktøy"
created = 2024-09-25
updated = 2025-02-28
+++

En slags liste over verktøy jeg bruker.

## Hardvare
- ASUS PN53
- Keychron Q1
- Macbook Pro

### Nyfiken på
- [Framework Laptop 13](https://frame.work/) -- venter bare på norsk lansering
- [Ploopy](https://ploopy.co) -- Gjør det selv, åpen ballmus
- [Fairphone](https://fairphone.com)
- [LightPhone](https://www.thelightphone.com/)

## Operativsystemer
- [NixOS](https://nixos.org/)
- [macOS](https://apple.com/macos)
- [Android](https://www.android.com/)

### Nyfiken på
- [Postmarket OS](https://postmarketos.org/)

## ~~Innvikling~~ Utvikling
- [Sublime Merge](https://www.sublimemerge.com/)
- [VSCode](https://code.visualstudio.com/)/[VSCodium](https://vscodium.com/)

### Nyfiken på
- [Zed](https://zed.dev/)

## Kommandolinje
- Emulator/klient: [Ghostty](https://ghostty.org/)
- Shell: [fish](https://fishshell.com/)
- Prompt: [starship](https://starship.rs/)

## Diverse
- Denne siden: [Zola](https://getzola.org)
- Hjernedynge: [Obsidian](https://obsidian.md/)
- Passordhåndterer: [Bitwarden](https://bitwarden.com)

## Internett
- Nettleser: [Firefox](https://firefox.com)
    - Adblock: [UBlock Origin](https://ublockorigin.com/)
- Søk: [Kagi](https://kagi.com)
- DNS: [NextDNS](https://nextdns.io)
- Epost: [Fastmail](https://fastmail.com)
- Epostklient: [Thunderbird](https://www.thunderbird.net/)

### Nyfiken på
- Nettleser: [zen browser](https://zen-browser.app/)
