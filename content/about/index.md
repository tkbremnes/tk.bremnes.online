+++
title = "Om tk.bremnes.online"
template = "page.html"
updated = 2025-02-28
+++

Programvaresmed på Nøtterøy.
Far til to, ektemann til en.
Jobber i Tønsberg hos [Spinner Labs](https://spinnerlabs.no).

- Aktiv i [Færder MDG](https://faerder.mdg.no/).
- Redigerer [Wikipedia](https://wikipedia.org)
- Ivrig D&D-spiller
- Pragmatisk idealist
- God på punktlister
- Dårlig på punktlighet
- Bygger LEGO


[CV (PDF)](/CV_Trond-Kjetil-Bremnes_2025-01-11.pdf)
