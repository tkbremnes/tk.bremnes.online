+++
title = "Endringslogg"
template = "page.html"
+++

#### 2025-01-11
- Legger til endringslogg (denne siden)
- Gjør siden mobilvennlig
- Fikser ødelagte lenker
- Legger til [veikart](/roadmap)
- Legger til [om](/about)

#### 2025-01-01
- Oppdateringer til stilsett

#### 2024-12-05
- legger til ny seksjon: [i dag lærte jeg](/til)

#### 2024-10-25
- legger til ny seksjon: [verktøy](/musings/tools)

#### 2024-07-13
- legger til (usynlig) [Mastodon link](https://mastodon.green/@tk) for verifisering på profil.
- mindre visuelle oppdateringer

#### 2024-05-08
- oppretter siden
