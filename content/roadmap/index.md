+++
title = "Veikart"
template = "page.html"
updated = 2025-01-11
+++

En liste over forbedringer til denne siden.
Ingen egentlig rekkefølge. Mer en merknad til meg selv om hva jeg tenker og har lyst til å gjøre her enn noe annet.

- [ ] Fylle ut siden [om meg](/about)
- [ ] Opprette RSS for [IDLJ](/til)
- [x] Gjøre siden mobilvennlig
- [ ] Fjerne lasting av font fra Google Fonts
- [ ] Fikse deploy-skript
- [ ] Automatisk deploy ved `git push`
- [ ] Implementere [catppuccin](https://catppuccin.com/)-tema
- [ ] Gjøre ferdig, gjøre robust footer til siden, som holder styr på når en side er opprettet og endret.
